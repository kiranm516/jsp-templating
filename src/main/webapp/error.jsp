<%@page language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Cheque Viewer - CCPC</title>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- As a link -->
                <header class="pb-3">
                    <nav class="navbar navbar-light bg-light">
                        <a class="navbar-brand" href="./">
                            <h4>Cheque Viewer</h4>
                        </a>
                    </nav>
                </header>

                <div class="card bg-light mb-3">
                    <div class="card-header">An error occured</div>
                    <div class="card-body">
                        <p class="card-text">Server encountered an error while processing this request. Please try after sometime</p>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <p class="text-muted text-right">CCPC Bangalore</p>
    </div>
</body>

</html>
