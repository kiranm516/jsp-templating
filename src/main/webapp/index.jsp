<%@page language="java" pageEncoding="UTF-8" %>
<%@ page isELIgnored ="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Cheque Viewer - CCPC</title>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <script src="js/moment.js"></script>
    <script src="js/tempusdominus-bootstrap-4.min.js"></script>
    <link rel="stylesheet" href="css/tempusdominus-bootstrap-4.min.css" />

    <script>
        $(function()    {
            $('#chequeDatePicker').datetimepicker({
                format: 'DD/MM/YYYY',
                maxDate: moment()
            });
        });
    </script>

</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- As a link -->
                <header class="pb-3">
                    <nav class="navbar navbar-light bg-light">
                        <a class="navbar-brand" href="./"><h4>Cheque Viewer</h4></a>
                    </nav>
                </header      
                <!-- Alert message -->
                <c:if test="${!empty alertMessage}">
                    <div class="alert alert-${alertMessage.alertMessageType} alert-dismissible fade show" role="alert">
                        ${alertMessage.alertMessageContent}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </c:if>

                <!-- Form -->
                <div class="input-form border-top border-bottom">
                    <div>
                    <div class="pt-2 pb-1">
                        <form class="form-inline" action="ViewCheque" method="GET">
                            <div class="form-group">
                                <label>Date:&nbsp;</label>
                                <input required type="text" size="10" name="date" class="form-control datetimepicker-input" id="chequeDatePicker" data-toggle="datetimepicker" data-target="#chequeDatePicker"/>                            
                            </div>
                            <div class="form-group mx-sm-3 mb-2">
                                <label>Cheque number:&nbsp;</label>
                                <input required type="number" name="chequeNumber" value="${chequeNumberInputValue}" size="6" class="form-control">
                            </div>
                            <button type="submit" class="btn btn-primary mb-2">View</button>
                        </form>
                    </div>
                    </div>
                </div>
                
                <c:if test="${!empty isChequesFound && isChequesFound}">
                <div class="pt-3 border border-light">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <c:forEach items="${chequeImagePaths}" var="chequeImagePath" varStatus="loop">
                                <c:choose>
                                    <c:when test="${loop.index == 0}">
                                        <li data-target="#carouselExampleIndicators" data-slide-to="${loop.index}" class="active"></li>
                                    </c:when>
                                    <c:otherwise>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="${loop.index}"></li>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>                            
                        </ol>
                        <div class="carousel-inner">
                            <c:forEach items="${chequeImagePaths}" var="chequeImagePath" varStatus="loop">
                                <c:choose>
                                <c:when test="${loop.index == 0}">
                                    <div class="carousel-item active">
                                        <img class="d-block w-100" src="${chequeImagePath}" alt="Cheque Image">
                                    </div>
                                </c:when>
                                <c:otherwise>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src="${chequeImagePath}" alt="Cheque Image">
                                    </div>
                                </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                </c:if>
            </div>
        </div>
        <div class="border-top mt-3 pt-2 pb-2">
            <p class="text-muted text-right">CCPC Bangalore</p>
        </div>
    </div>
</body>

</html>
